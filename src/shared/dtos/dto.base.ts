import { BaseEntity } from 'typeorm';

export abstract class BaseDTO implements Readonly<BaseDTO> {
  protected abstract from(dto: any);

  abstract fromEntity(entity: BaseEntity);

  abstract toEntity(): BaseEntity;
}
