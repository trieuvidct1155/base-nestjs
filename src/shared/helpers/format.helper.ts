export function FORMAT_ID_FN(id = '') {
  return '......' + id.slice(id.length - 6);
}

export function FORMAT_CURRENCY_FN(num: number) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
