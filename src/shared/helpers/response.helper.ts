import { HttpStatus } from '@nestjs/common';

export class ResponseData<T> {
  error: boolean;
  errorKey: string;
  message: string;
  data: T | T[];
  httpCode: number;

  constructor({ error, errorKey, data, httpCode, message }: { error?: boolean; errorKey?: string; data?: T; httpCode?: number; message?: string }) {
    this.error = error || false;
    this.errorKey = errorKey;
    this.message = message;
    this.data = data;
    this.httpCode = httpCode ? httpCode : error ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK;

    if (error && data) this.message = 'something_went_wrong_response_data';
  }
}

const myMathRound = (num: number) => num + (num - num > 0 ? 1 : 0);

/**
 *	CREATE ARRAY OF PAGINATION LINKS
 * @param {*} total
 * @param {*} limit
 * @param {*} url
 * @returns LINKS ARRAY
 */
export const PAGINATION = (total: any = 0, limit: any = 0, url: any = '', oldQuery: any = '') => {
  const pagination = [];

  for (let i = 1; i <= myMathRound(total / limit); i++) {
    pagination.push(url + oldQuery.replace(/&page=\d*|page=\d*/, '') + '&page=' + i);
  }

  return pagination;
};
