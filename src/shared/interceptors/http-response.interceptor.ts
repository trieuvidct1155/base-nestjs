import { Injectable, NestInterceptor, ExecutionContext, CallHandler, HttpStatus } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class ResponseFormat<T> {
  @ApiProperty()
  error: boolean;
  @ApiProperty()
  errorKey?: string;
  @ApiProperty()
  httpCode?: HttpStatus;
  @ApiProperty()
  message: string;
  @ApiProperty()
  data: T;
  @ApiProperty()
  duration?: string;
}

export class ReturnController<T> {
  @ApiProperty()
  message: string;
  @ApiProperty()
  data: T;
  @ApiProperty()
  httpCode?: HttpStatus;
}

@Injectable()
export class ResponseInterceptor<T> implements NestInterceptor<T, ResponseFormat<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<ResponseFormat<T>> {
    const now = Date.now();
    const httpContext = context.switchToHttp();
    const request = httpContext.getRequest();

    return next.handle().pipe(
      map(data => ({
        error: data?.error || false,
        data: data.data,
        message: data.message,
        path: request.path,
        duration: `${Date.now() - now}ms`,
      })),
    );
  }
}
