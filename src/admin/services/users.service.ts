import { BaseService } from '@/shared/services/base.service';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { UserRepository } from '../repositories/users.repo';

@Injectable()
export class UsersService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    userRepository: UserRepository,
  ) {
    super(userRepository, User, UsersService.name);
  }
}
