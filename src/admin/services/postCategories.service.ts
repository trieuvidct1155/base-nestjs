import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '@/shared/services/base.service';
import { PostCategory } from '../entities/postCategory.entity';
import { PostCategoryRepository } from '../repositories/postCategories.repo';

@Injectable()
export class PostCategoriesService extends BaseService<PostCategory> {
  constructor(
    @InjectRepository(PostCategory)
    postCateRepository: PostCategoryRepository,
  ) {
    super(postCateRepository, PostCategory, PostCategoriesService.name);
  }
}
