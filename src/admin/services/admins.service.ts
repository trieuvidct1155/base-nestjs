import { Admin } from './../entities/admin.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '@/shared/services/base.service';
import * as bcrypt from 'bcrypt';
import { AdminRepository } from '../repositories/admins.repo';

@Injectable()
export class AdminService extends BaseService<Admin> {
  constructor(
    @InjectRepository(Admin)
    adminRepository: AdminRepository,
  ) {
    super(adminRepository, Admin, AdminService.name);
  }

  async login({ username, password }) {
    const signalAdmin = await this.findOne({
      where: {
        username,
      },
    });
    const user: Admin = signalAdmin.data as Admin;

    if (!user) return null;
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) return null;

    return user;
  }

  async register(body) {
    const hashPassword = await bcrypt.hash(body.password, +process.env?.BYCRYPT_SALT || 5);
    const accountAdmin = await this.create({ ...body, password: hashPassword });

    return accountAdmin;
  }
}
