import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { User } from '../entities/user.entity';

export class UserDto implements Readonly<UserDto> {
  @ApiProperty({ required: true })
  @IsString()
  username: string;

  @ApiProperty({ required: true })
  @IsString()
  email: string;

  @ApiProperty({ required: true })
  @IsString()
  password: string;

  protected static from(dto: Partial<UserDto>) {
    const it = new UserDto();
    it.username = dto.username;
    it.email = dto.email;
    it.password = dto.password;
    return it;
  }

  public static fromEntity(entity: User) {
    return this.from({
      username: entity.username,
      email: entity.email,
      password: entity.password,
    });
  }

  public toEntity() {
    const it = new User();
    it.username = this.username;
    it.email = this.email;
    it.password = this.password;
    it.createdDateTime = new Date();
    return it;
  }
}
