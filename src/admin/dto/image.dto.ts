import { ApiProperty } from '@nestjs/swagger';
import { MaxFileSize, MemoryStoredFile } from 'nestjs-form-data';

export class CreateImageDto implements Readonly<CreateImageDto> {
  @ApiProperty({ required: true })
  @MaxFileSize(1e6, { each: true })
  images: MemoryStoredFile[];
}
