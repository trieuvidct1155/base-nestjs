import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { IsFile, MaxFileSize, MemoryStoredFile } from 'nestjs-form-data';

export class CreatePostCategoryDto implements Readonly<CreatePostCategoryDto> {
  @ApiProperty({ required: true })
  @IsString()
  name: string;

  @ApiProperty({ required: true })
  status: string;

  @ApiProperty({ required: false })
  description: string;

  @ApiProperty({ required: true })
  slug: string;

  @ApiProperty({ required: false })
  content: string;

  @ApiProperty({ required: false })
  seoDescription: string;

  @ApiProperty({ required: true })
  seoTitle: string;

  @ApiProperty({ required: false })
  seoSchema: string;

  @ApiProperty({ required: false })
  seoKeywords: string;

  @ApiProperty({ required: false })
  seoCanonical: string;

  @ApiProperty({ required: true })
  @IsFile()
  @MaxFileSize(1e6)
  thumnail: MemoryStoredFile;

  @ApiProperty({ required: false })
  @IsFile()
  @MaxFileSize(1e6)
  banner: MemoryStoredFile;
}
