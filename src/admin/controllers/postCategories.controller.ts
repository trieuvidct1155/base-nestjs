import { PostCategory } from './../entities/postCategory.entity';
import { Controller } from '@nestjs/common';
import { PostCategoriesService } from '../services/postCategories.service';
import { BaseController } from '@/shared/controllers/base.controller';
import { ApiTags } from '@nestjs/swagger';

@Controller('post-categories')
@ApiTags('Admin PostCategory')
export class PostCategoriesController extends BaseController<PostCategory> {
  constructor(postCateoryService: PostCategoriesService) {
    super(postCateoryService, PostCategoriesController.name);
  }
}
