import { BaseController } from '@/shared/controllers/base.controller';
import { User } from '../entities/user.entity';
import { Controller } from '@nestjs/common';
import { UsersService } from '../services/users.service';

@Controller('users')
export class UsersController extends BaseController<User> {
  constructor(userService: UsersService) {
    super(userService, UsersController.name);
  }
}
