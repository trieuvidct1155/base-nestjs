import { CacheInterceptor, Module } from '@nestjs/common';
import { UsersController } from './controllers/users.controller';
import { BaseEntityClass } from '@/shared/entities/base.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Image } from './entities/image.entity';
import { UsersService } from './services/users.service';
import { PostCategoriesService } from './services/postCategories.service';
import { PostCategoriesController } from './controllers/postCategories.controller';
import { PostCategory } from './entities/postCategory.entity';
import { LoginController } from './controllers/login.controller';
import { ImageController } from './controllers/image.controller';
import { ImagesService } from './services/images.service';
import { FileUtils } from '@/shared/utils/file';
import { Admin } from './entities/admin.entity';
import { AdminService } from './services/admins.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './passport/local.strategy';

@Module({
  imports: [TypeOrmModule.forFeature([Admin, User, Image, PostCategory]), PassportModule],
  controllers: [UsersController, PostCategoriesController, LoginController, ImageController],
  providers: [LocalStrategy, AdminService, UsersService, PostCategoriesService, ImagesService, BaseEntityClass, FileUtils, CacheInterceptor],
})
export class AdminModule {}
