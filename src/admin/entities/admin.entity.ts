import { Column, Entity, Index } from 'typeorm';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { USER_ROLE } from '../enum';
import { BaseEntityClass } from '@/shared/entities/base.entity';

@Entity('Admin')
export class Admin extends BaseEntityClass {
  @Column({ name: 'user_name', type: 'varchar' })
  @IsString()
  @IsNotEmpty()
  @Index({ unique: true })
  username: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'integer' })
  role: USER_ROLE;

  @Column({ name: 'full_name', type: 'varchar' })
  fullName: string;

  @Column({ type: 'varchar', length: 100 })
  @IsEmail()
  email: string;
}
