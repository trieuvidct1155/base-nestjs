import { BaseEntityClass } from '@/shared/entities/base.entity';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { IsNotEmpty, IsString } from 'class-validator';
import { Image } from './image.entity';
import { PostCategory } from './postCategory.entity';

@Entity('Post')
export class Post extends BaseEntityClass {
  @Column({ name: 'name', type: 'varchar' })
  @Index({ unique: true })
  name: string;

  @Column({ name: 'status' })
  @IsString()
  @IsNotEmpty()
  @Index()
  status: string;

  @Column({ name: 'description' })
  description: string;

  @Column({ name: 'slug', type: 'varchar', unique: true })
  @Index({ fulltext: true })
  slug: string;

  @Column({ name: 'content', type: 'varchar' })
  content: string;

  @Column({ name: 'seo_description' })
  seoDescription: string;

  @Column({ name: 'seo_title' })
  seoTitle: string;

  @Column({ name: 'seo_schema' })
  seoSchema: string;

  @Column({ name: 'seo_keywords' })
  seoKeywords: string;

  @Column({ name: 'seo_canonical' })
  seoCanonical: string;

  @ManyToOne(() => Image, image => image.postCategories)
  @JoinColumn({ name: 'image_id_1', referencedColumnName: 'id' })
  thumbnail: Image;

  @ManyToOne(() => Image, image => image.postCategories)
  @JoinColumn({ name: 'image_id_2', referencedColumnName: 'id' })
  banner: Image;

  @ManyToOne(() => PostCategory, postCategory => postCategory.children)
  postCategory: PostCategory;

  @Column('text', { array: true })
  tags: string[];

  @Column({ type: 'integer' })
  views: number;

  @Column()
  author: string;

  @Column()
  toc: string;
}
