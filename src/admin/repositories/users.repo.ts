import { BaseRepository } from '@/shared/repositories/base.repo';
import { User } from '@/admin/entities';

export class UserRepository extends BaseRepository<User> {}
