import { BaseRepository } from '@/shared/repositories/base.repo';
import { Post } from '@/admin/entities';

export class PostRepository extends BaseRepository<Post> {}
