import { BaseRepository } from '@/shared/repositories/base.repo';
import { Image } from '@/admin/entities';

export class ImageRepository extends BaseRepository<Image> {}
