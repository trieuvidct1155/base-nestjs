import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1670043824187 implements MigrationInterface {
  name = 'init1670043824187';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "Admin" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "user_name" character varying NOT NULL, "password" character varying NOT NULL, "role" integer NOT NULL, "full_name" character varying NOT NULL, "email" character varying(100) NOT NULL, CONSTRAINT "PK_3a489f4a44372ff150d7924dc3d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_c7df844f044be1c5785f2c35fa" ON "Admin" ("user_name") `);
    await queryRunner.query(
      `CREATE TABLE "Doc" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "name" character varying NOT NULL, "src" character varying NOT NULL, "type" character varying NOT NULL, CONSTRAINT "PK_2be41af14f8559cea945e7e3393" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "User" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "user_name" character varying NOT NULL, "password" character varying NOT NULL, "full_name" character varying NOT NULL, "address" character varying NOT NULL, "phone" character varying NOT NULL, "occupation " character varying NOT NULL, "email" character varying(100) NOT NULL, CONSTRAINT "PK_9862f679340fb2388436a5ab3e4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_a78eae3f503a4456f9b7ea7448" ON "User" ("user_name") `);
    await queryRunner.query(
      `CREATE TABLE "Post" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "name" character varying NOT NULL, "status" character varying NOT NULL, "description" character varying NOT NULL, "slug" character varying NOT NULL, "content" character varying NOT NULL, "seo_description" character varying NOT NULL, "seo_title" character varying NOT NULL, "seo_schema" character varying NOT NULL, "seo_keywords" character varying NOT NULL, "seo_canonical" character varying NOT NULL, "tags" text array NOT NULL, "views" integer NOT NULL, "author" character varying NOT NULL, "toc" character varying NOT NULL, "image_id_1" integer, "image_id_2" integer, "postCategoryId" integer, CONSTRAINT "UQ_15966f17c69be96c54100a42940" UNIQUE ("slug"), CONSTRAINT "PK_c4d3b3dcd73db0b0129ea829f9f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_7754fb9fb40a3eb94ecf58b9df" ON "Post" ("name") `);
    await queryRunner.query(`CREATE INDEX "IDX_a20dec4e74e25a36ec2525b7da" ON "Post" ("status") `);
    await queryRunner.query(`CREATE INDEX "IDX_15966f17c69be96c54100a4294" ON "Post" ("slug") `);
    await queryRunner.query(
      `CREATE TABLE "PostCategory" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "name" character varying NOT NULL, "status" character varying NOT NULL, "description" character varying NOT NULL, "slug" character varying NOT NULL, "content" character varying NOT NULL, "seo_description" character varying NOT NULL, "seo_title" character varying NOT NULL, "seo_schema" character varying NOT NULL, "seo_keywords" character varying NOT NULL, "seo_canonical" character varying NOT NULL, "image_id_1" integer, "image_id_2" integer, "parentId" integer, CONSTRAINT "UQ_44602c3fc3c5b14325b4d77e66c" UNIQUE ("slug"), CONSTRAINT "PK_c7c710127b4ff8b63fe44b1498d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE UNIQUE INDEX "IDX_ea5700023bce97805f1f2a8563" ON "PostCategory" ("name") `);
    await queryRunner.query(`CREATE INDEX "IDX_ffa64ec079452b291ba7b9435e" ON "PostCategory" ("status") `);
    await queryRunner.query(`CREATE INDEX "IDX_44602c3fc3c5b14325b4d77e66" ON "PostCategory" ("slug") `);
    await queryRunner.query(
      `CREATE TABLE "Image" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "name" character varying NOT NULL, "alt" character varying NOT NULL, "src" character varying NOT NULL, CONSTRAINT "PK_ddecd6b02f6dd0d3d10a0a74717" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "PageComponent" ("id" SERIAL NOT NULL, "created_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "created_by" character varying(300) DEFAULT 'admin', "last_changed_date_time" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "last_changed_by" character varying(300), "name" character varying NOT NULL, "page" character varying NOT NULL, "seo_schema" character varying NOT NULL, "seo_canonical" character varying NOT NULL, "titles" text array NOT NULL, "contents" text array NOT NULL, CONSTRAINT "PK_53f671de78421c21fc755c253bb" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_5994dc5556d277161b526459d1" ON "PageComponent" ("name") `);
    await queryRunner.query(`CREATE INDEX "IDX_d2e0005d26fda3138f687d2d82" ON "PageComponent" ("page") `);
    await queryRunner.query(
      `CREATE TABLE "image_pageComponent" ("imageId" integer NOT NULL, "pageComponentId" integer NOT NULL, CONSTRAINT "PK_09aa36126911ad9f9ea90b08d72" PRIMARY KEY ("imageId", "pageComponentId"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_220effb3656ff4fafc79ec776a" ON "image_pageComponent" ("imageId") `);
    await queryRunner.query(`CREATE INDEX "IDX_02a28571f59012d5d3974fed01" ON "image_pageComponent" ("pageComponentId") `);
    await queryRunner.query(
      `CREATE TABLE "doc_pageComponent" ("docId" integer NOT NULL, "pageComponentId" integer NOT NULL, CONSTRAINT "PK_fe342e5d3a0dbea62660073314d" PRIMARY KEY ("docId", "pageComponentId"))`,
    );
    await queryRunner.query(`CREATE INDEX "IDX_320e83fcdfafd10723feccc438" ON "doc_pageComponent" ("docId") `);
    await queryRunner.query(`CREATE INDEX "IDX_219cf711ae70a0ddda1c75dcf1" ON "doc_pageComponent" ("pageComponentId") `);
    await queryRunner.query(
      `ALTER TABLE "Post" ADD CONSTRAINT "FK_72d788792f15640bdab53122cf5" FOREIGN KEY ("image_id_1") REFERENCES "Image"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Post" ADD CONSTRAINT "FK_38ea8b771d6c9be8d41e0eac6c0" FOREIGN KEY ("image_id_2") REFERENCES "Image"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "Post" ADD CONSTRAINT "FK_86590617c1b362ca2acfc313a5b" FOREIGN KEY ("postCategoryId") REFERENCES "PostCategory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "PostCategory" ADD CONSTRAINT "FK_bce3f5aa92e98bab933ffc8f36a" FOREIGN KEY ("image_id_1") REFERENCES "Image"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "PostCategory" ADD CONSTRAINT "FK_6df9fcf406f83d5d7ff235b7083" FOREIGN KEY ("image_id_2") REFERENCES "Image"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "PostCategory" ADD CONSTRAINT "FK_6ef21abcf19791c4641a399c083" FOREIGN KEY ("parentId") REFERENCES "PostCategory"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "image_pageComponent" ADD CONSTRAINT "FK_220effb3656ff4fafc79ec776ae" FOREIGN KEY ("imageId") REFERENCES "PageComponent"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "image_pageComponent" ADD CONSTRAINT "FK_02a28571f59012d5d3974fed013" FOREIGN KEY ("pageComponentId") REFERENCES "Image"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "doc_pageComponent" ADD CONSTRAINT "FK_320e83fcdfafd10723feccc438d" FOREIGN KEY ("docId") REFERENCES "PageComponent"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "doc_pageComponent" ADD CONSTRAINT "FK_219cf711ae70a0ddda1c75dcf1a" FOREIGN KEY ("pageComponentId") REFERENCES "Doc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "doc_pageComponent" DROP CONSTRAINT "FK_219cf711ae70a0ddda1c75dcf1a"`);
    await queryRunner.query(`ALTER TABLE "doc_pageComponent" DROP CONSTRAINT "FK_320e83fcdfafd10723feccc438d"`);
    await queryRunner.query(`ALTER TABLE "image_pageComponent" DROP CONSTRAINT "FK_02a28571f59012d5d3974fed013"`);
    await queryRunner.query(`ALTER TABLE "image_pageComponent" DROP CONSTRAINT "FK_220effb3656ff4fafc79ec776ae"`);
    await queryRunner.query(`ALTER TABLE "PostCategory" DROP CONSTRAINT "FK_6ef21abcf19791c4641a399c083"`);
    await queryRunner.query(`ALTER TABLE "PostCategory" DROP CONSTRAINT "FK_6df9fcf406f83d5d7ff235b7083"`);
    await queryRunner.query(`ALTER TABLE "PostCategory" DROP CONSTRAINT "FK_bce3f5aa92e98bab933ffc8f36a"`);
    await queryRunner.query(`ALTER TABLE "Post" DROP CONSTRAINT "FK_86590617c1b362ca2acfc313a5b"`);
    await queryRunner.query(`ALTER TABLE "Post" DROP CONSTRAINT "FK_38ea8b771d6c9be8d41e0eac6c0"`);
    await queryRunner.query(`ALTER TABLE "Post" DROP CONSTRAINT "FK_72d788792f15640bdab53122cf5"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_219cf711ae70a0ddda1c75dcf1"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_320e83fcdfafd10723feccc438"`);
    await queryRunner.query(`DROP TABLE "doc_pageComponent"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_02a28571f59012d5d3974fed01"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_220effb3656ff4fafc79ec776a"`);
    await queryRunner.query(`DROP TABLE "image_pageComponent"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_d2e0005d26fda3138f687d2d82"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_5994dc5556d277161b526459d1"`);
    await queryRunner.query(`DROP TABLE "PageComponent"`);
    await queryRunner.query(`DROP TABLE "Image"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_44602c3fc3c5b14325b4d77e66"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_ffa64ec079452b291ba7b9435e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_ea5700023bce97805f1f2a8563"`);
    await queryRunner.query(`DROP TABLE "PostCategory"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_15966f17c69be96c54100a4294"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_a20dec4e74e25a36ec2525b7da"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_7754fb9fb40a3eb94ecf58b9df"`);
    await queryRunner.query(`DROP TABLE "Post"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_a78eae3f503a4456f9b7ea7448"`);
    await queryRunner.query(`DROP TABLE "User"`);
    await queryRunner.query(`DROP TABLE "Doc"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_c7df844f044be1c5785f2c35fa"`);
    await queryRunner.query(`DROP TABLE "Admin"`);
  }
}
