terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.44.0"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region = var.REGION

  // Set up tags that all resources will be tagged with
  default_tags {
    tags = {
      Domain      = var.DOMAIN
      Environment = terraform.workspace
      ManagedBy   = "Terraform"
    }
  }
}
