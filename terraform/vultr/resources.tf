# Find the ID of an existing SSH key.
data "vultr_ssh_key" "primary" {
  filter {
    name   = "name"
    values = ["pc-ws"]
  }
}
