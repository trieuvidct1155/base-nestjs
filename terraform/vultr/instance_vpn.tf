// Create a Vultr virtual machine.
resource "vultr_instance" "instance-vpn" {
  region = var.REGION
  plan   = var.PLAN
  os_id  = var.OS_ID
  # snapshot_id  = "b615fffff69"
  ssh_key_ids = ["${data.vultr_ssh_key.primary.id}"]
  hostname    = "instance-vpn"
  #  startup_script_id = "ubuntu-ansible-ready"
  tags              = ["ubuntu20"]
  firewall_group_id = vultr_firewall_group.base-group.id

  lifecycle {
    create_before_destroy = "true"
  }
}
